data "scaleway_image" "ubuntu" {
    architecture    = "x86_64"
    name            = "Ubuntu Bionic"
}

# Ugly hack to bypass the image bug
variable "scw_ubuntu_id" {
    type = "string"
    default = "d4067cdc-dc9d-4810-8a26-0dae51d7df42"
}

resource "scaleway_ip" "k8s_master_ips" {
    count = "${var.k8s_master_count}"
}

resource "scaleway_ip" "k8s_worker_ips" {
    count = "${var.k8s_worker_count}"
}

resource "scaleway_server" "k8s_masters" {
    name = "master_${count.index}"
    count = "${var.k8s_master_count}"
    image = "${var.scw_ubuntu_id}"
    public_ip = "${element(scaleway_ip.k8s_master_ips.*.ip, count.index)}"
    state = "running"
    type = "${var.k8s_master_type}"
    tags = ["kube-master", "etcd", "k8s-cluster"]
}

resource "scaleway_server" "k8s_workers" {
    name = "worker_${count.index}"
    count = "${var.k8s_worker_count}"
    image = "${var.scw_ubuntu_id}"
    public_ip = "${element(scaleway_ip.k8s_worker_ips.*.ip, count.index)}"
    state = "running"
    type = "${var.k8s_worker_type}"
    tags = ["kube-node", "k8s-cluster"]
}

output "master_public_ip" {
    value = "${scaleway_server.k8s_masters.*.public_ip}"
}

output "worker_public_ip" {
    value = "${scaleway_server.k8s_workers.*.public_ip}"
}
