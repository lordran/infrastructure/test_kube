variable "k8s_master_type" {
    default = "START1-S"
}

variable "k8s_master_count" {
    default = "3"
}

variable "k8s_worker_type" {
    default = "START1-S"
}

variable "k8s_worker_count" {
    default = "3"
}
