```
git submodule update --init --recursive
pip3 install -r 3d/kubespray/requirements.txt -r requirements.txt
ansible-galaxy install -r roles/requirements.yml
ansible-playbook -i inventory/scaleway_inventory.yml kubernetes.yml
```
